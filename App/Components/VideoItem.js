import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import { nFormatter } from '../Lib/Formatter';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from './Styles/VideoItemStyle';

export default class VideoItem extends Component {
    render() {
        let video = this.props.video;
        return (
            <View style={styles.container}>
                <Image source={{ uri: video.snippet.thumbnails.medium.url }} style={{ height: 200 }} />
                <View style={styles.descContainer}>
                    <Image source={{ uri: video.snippet.publisher.logoUrl }} style={{ width: 50, height: 50, borderRadius: 25 }} />
                    <View style={styles.videoDetails}>
                        <Text numberOfLines={2} style={styles.videoTitle}>{video.snippet.title}</Text>
                        <Text style={styles.videoStats}>{video.snippet.publisher.name + " · " + nFormatter(video.statistics.viewCount, 1) + " · 3 months ago "}</Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name="more-vert" size={20} color="#999999"/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
