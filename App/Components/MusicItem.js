import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import { nFormatter } from '../Lib/Formatter';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from './Styles/MusicItemStyle';

export default class MusicItem extends Component {
    render() {
        let music = this.props.music;
        return (
            <View style={styles.container}>
                <View style={styles.descContainer}>
                    <Image source={{ uri: music.snippet.thumbnails.medium.url }} style={styles.musicThumbnail} />
                    <View style={styles.musicDetails}>
                        <Text numberOfLines={2} style={styles.musicTitle}>{music.snippet.title}</Text>
                        <View style={styles.musicDetailExtra}>
                          <Image source={{ uri: music.snippet.publisher.logoUrl }} style={styles.publisherThumbnail} />
                          <Text style={styles.musicStats}>{music.snippet.publisher.name + " · " + nFormatter(music.statistics.viewCount, 1)}</Text>
                        </View>
                    </View>
                    <TouchableOpacity>
                        <Icon name="more-vert" size={20} color="#999999"/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
