import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    padding: 15
  },
  descContainer: {
      flexDirection: 'row',
      paddingTop: 15
  },
  videoTitle: {
      fontSize: 16,
      color: '#3c3c3c'
  },
  videoDetails: {
      paddingHorizontal: 15,
      flex: 1
  },
  videoStats: {
      fontSize: 15,
      paddingTop: 3
  }
})
