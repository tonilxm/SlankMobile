import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    padding: 15
  },
  descContainer: {
      flexDirection: 'row',
      paddingTop: 15
  },
  musicThumbnail: {
    height: 60,
    width: 60
  },
  musicTitle: {
      fontSize: 16,
      color: '#3c3c3c'
  },
  musicDetails: {
      paddingHorizontal: 15,
      flex: 1
  },
  musicDetailExtra: {
    flexDirection: 'row'
  },
  publisherThumbnail: { width: 20, height: 20, borderRadius: 5, marginRight: 5 },
  musicStats: {
      fontSize: 15,
      paddingTop: 3
  }
})
