import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native'
import { View, Text } from 'native-base'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Styles from './Styles/FooterStyle'

export default class Footer extends Component {
  render () {
    return (
      <View style={Styles.tabBar}>
      <TouchableOpacity style={Styles.tabItem}>
        <Icon name="home" size={25}/>
        <Text style={Styles.tabTitle}>Home</Text>
      </TouchableOpacity>
      <TouchableOpacity style={Styles.tabItem}>
        <Icon name="whatshot" size={25} />
        <Text style={Styles.tabTitle}>Trending</Text>
      </TouchableOpacity>
      <TouchableOpacity style={Styles.tabItem}>
        <Icon name="subscriptions" size={25} />
        <Text style={Styles.tabTitle}>Subscriptions</Text>
      </TouchableOpacity>
      <TouchableOpacity style={Styles.tabItem}>
        <Icon name="folder" size={25} />
        <Text style={Styles.tabTitle}>Library</Text>
      </TouchableOpacity>
    </View>
    )
  }
}
