import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { Item, Input } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MusicItem from '../Components/MusicItem';
import data from '../Fixtures/musicData.json';
import { Images } from "../Themes";
import Styles from './Styles/MusicPlaylistScreenStyle';
import Footer from '../Components/Footer';

export default class MusicPlaylistScreen extends Component {
  handleSearch = () => {
    alert('Test');
    console.log("Test");
  }

  render() {
    return (
      <View style={Styles.container}>
        <View style={Styles.navBar}>
          <Image source={Images.aerosmithLogo} style={{ width: 98, resizeMode: 'contain' }} />
          <View style={Styles.rightNav}>
            <TouchableOpacity style={{ flexGrow: 1 }}>
              <Item>
                <Input placeholder='Search Music Here'/>
                <Icon style={Styles.navItem} name="search" size={25} onPress={this.handleSearch}/>
              </Item>
            </TouchableOpacity>
          </View>
        </View>
        <View style={Styles.body}>
          <FlatList
          data={data.items}
          renderItem={(music)=><MusicItem music={music.item} />}
          keyExtractor={(item)=>item.id}
          ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
           />
        </View>
        <Footer/>
      </View>
    );
  }
}

