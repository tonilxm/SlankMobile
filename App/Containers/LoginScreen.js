import React, { PropTypes } from "react";
import { View, ScrollView, Text, TouchableWithoutFeedback, Image, Keyboard, LayoutAnimation } from "react-native";
import { connect } from "react-redux";
import Styles from "./Styles/LoginScreenStyles";
import { Images, Metrics, Fonts } from "../Themes";
import LoginActions from "../Redux/LoginRedux";
import { Button as NBButton, Text as NBText, Form, Item, Input, Label } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";

class LoginScreen extends React.Component {
  static propTypes = {
		dispatch: PropTypes.func,
		fetching: PropTypes.bool,
		attemptLogin: PropTypes.func,
	};

	isAttempting = false;
	keyboardDidShowListener = {};
	keyboardDidHideListener = {};

	constructor(props) {
		super(props);
		this.state = {
			username: "toni.xm@gmail.com",
			password: "passwordkece",
			visibleHeight: Metrics.screenHeight,
			topLogo: { width: Metrics.screenWidth - 40, height: Metrics.screenHeight * 0.4 }
		};
		this.isAttempting = false;
	}

	componentWillReceiveProps(newProps) {
		this.forceUpdate();
		// Did the login attempt complete?
		if (this.isAttempting && !newProps.fetching) {
			this.props.navigation.goBack();
		}
	}

	componentWillMount() {
		// Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
		// TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
		this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this.keyboardDidShow);
		this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this.keyboardDidHide);
	}

	componentWillUnmount() {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	keyboardDidShow = e => {
		// Animation types easeInEaseOut/linear/spring
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
		let newSize = Metrics.screenHeight - e.endCoordinates.height;
		this.setState({
			visibleHeight: newSize,
			topLogo: { width: 200, height: 100 },
		});
	};

	keyboardDidHide = e => {
		// Animation types easeInEaseOut/linear/spring
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
		this.setState({
			visibleHeight: Metrics.screenHeight,
			topLogo: { width: Metrics.screenWidth - 40, height: Metrics.screenHeight * 0.4 },
		});
	};

	handlePressLogin = () => {
		// const { username, password } = this.state
		// this.isAttempting = true
		// attempt a login - a saga is listening to pick it up from here.
		// this.props.attemptLogin(username, password);
		this.props.navigation.navigate("MusicPlaylistScreen");
	};

  hideKeboard = () => {
    Keyboard.dismiss();
  };

	handleChangeUsername = text => {
		this.setState({ username: text });
	};

	handleChangePassword = text => {
		this.setState({ password: text });
	};

	render() {
		const { username, password } = this.state;
		const { fetching } = this.props;
		const editable = !fetching;
		const textInputStyle = editable ? Styles.textInput : Styles.textInputReadonly;
		return (
      <ScrollView
        contentContainerStyle={{ justifyContent: "center" }}
        style={ Styles.container }
        keyboardShouldPersistTaps="always">
        <TouchableWithoutFeedback onPress={this.hideKeboard}>
          <Image source={Images.aerosmithLogo} style={[Styles.topLogo, this.state.topLogo]}/>
        </TouchableWithoutFeedback>
        <View style={ Styles.form }>
          <Form>
            <Item>
              <Icon name="user" style={{ fontSize: 18, marginRight: 8 }}/>
              <Input
                ref="username"
                value={username}
                editable={editable}
                keyboardType="default"
                returnKeyType="next"
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={this.handleChangeUsername}
                underlineColorAndroid="transparent"
                onSubmitEditing={() => this.password._root.focus()}
                placeholder="Username"
              />
            </Item>
            <Item>
              <Icon name="lock" style={{ fontSize: 18, marginRight: 8 }}/>
              <Input
                ref={ref => (this.password = ref)}
                value={password}
                editable={editable}
                keyboardType="default"
                returnKeyType="go"
                autoCapitalize="none"
                autoCorrect={false}
                secureTextEntry
                onChangeText={this.handleChangePassword}
                underlineColorAndroid="transparent"
                onSubmitEditing={this.handlePressLogin}
                placeholder="Password"
              />
            </Item>
          </Form>
          <View style={{ marginTop: 5 }}>
            <NBText style={{ alignSelf:"flex-end", marginRight: 10, fontSize: Fonts.size.small }}>Forgot password?</NBText>
          </View>
          <View style={[Styles.loginRow]}>
            <NBButton block style={{ alignSelf:"center" }} onPress={this.handlePressLogin}>
              <NBText>Sign In</NBText>
            </NBButton>
          </View>
          <View style={[Styles.socialLoginRow]}>
            <NBButton block style={{ alignSelf:"center", marginRight:5, backgroundColor: '#3B5998' }} onPress={this.hideKeboard}>
              <Icon name="facebook" size={Fonts.size.medium} color='white'>
                <Text> Login Facebook</Text>
              </Icon>
            </NBButton>
            <NBButton block style={{ alignSelf:"center", backgroundColor: '#1DA1F2' }} onPress={this.hideKeboard}>
              <Icon name="twitter" size={Fonts.size.medium} color='white'>
                <Text> Login Twitter</Text>
              </Icon>
            </NBButton>
          </View>
          <View style={[Styles.registerRow]}>
            <NBText>Don't have account?</NBText>
            <NBText>Register</NBText>
          </View>
        </View>
			</ScrollView>
		);
	}
}

const mapStateToProps = state => {
	return {
		fetching: state.login.fetching,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		attemptLogin: (username, password) => dispatch(LoginActions.loginRequest(username, password)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
