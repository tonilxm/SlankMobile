import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    paddingTop: Metrics.baseMargin,
    backgroundColor: Colors.snow
  },
  navBar: {
    height: 55,
    backgroundColor: Colors.snow,
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  rightNav: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1
  },
  navItem: {
    marginLeft: 25
  },
  body: {
    flex: 1
  },
})
