import { Colors, Metrics } from "../../Themes"

export default {
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: Colors.backgroundColor
  },
  imageContainer: {
    justifyContent: 'center',
    height: Metrics.screenHeight * 0.3,
    
  },
  logo: {
    width: 200,
    resizeMode: 'contain'
  }
}
