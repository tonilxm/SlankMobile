import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { Item, Input } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from '../Components/VideoItem';
import data from '../Fixtures/videoData.json';
import { Images, Metrics, Fonts } from "../Themes";
import Styles from './Styles/VideoPlaylistScreenStyle';
import Footer from '../Components/Footer';

export default class VideoPlaylistScreen extends Component {
  handleSearch = () => {
    alert('Test');
    console.log("Test");
  }

  render() {
    return (
      <View style={Styles.container}>
        <View style={Styles.navBar}>
          <Image source={Images.aerosmithLogo} style={{ width: 98, resizeMode: 'contain' }} />
          <View style={Styles.rightNav}>
            <TouchableOpacity style={{ flexGrow: 1 }}>
              <Item>
                <Input placeholder='Search here'/>
                <Icon style={Styles.navItem} name="search" size={25} onPress={this.handleSearch}/>
              </Item>
            </TouchableOpacity>
          </View>
        </View>
        <View style={Styles.body}>
          <FlatList
          data={data.items}
          renderItem={(video)=><VideoItem video={video.item} />}
          keyExtractor={(item)=>item.id}
          ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
           />
        </View>
        <Footer/>
      </View>
    );
  }
}

