import React from "react";
import { DrawerNavigator } from "react-navigation";
import MusicPlaylistScreen from '../Containers/MusicPlaylistScreen'
import VideoPlaylistScreen from '../Containers/VideoPlaylistScreen'
import DrawerContent from "../Containers/DrawerContent";

import LoginScreen from "../Containers/LoginScreen";

const NavigationDrawer = DrawerNavigator({
  MusicPlaylistScreen: { screen: MusicPlaylistScreen },
    VideoPlaylistScreen: { screen: VideoPlaylistScreen }
	},
	{
		initialRouteName: "VideoPlaylistScreen",
		contentComponent: props => <DrawerContent {...props} />,
	}
);

export default NavigationDrawer;
